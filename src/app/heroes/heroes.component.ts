import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero'; // <-- importa la classe creata 

import { HeroService } from '../hero.service';
// collega lo stile dei component al file css presente
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {
  /* Abbiamo aggiunto nella classe HeroesComponent un oggeto 
  hero importando la classe creata in precedenza, inizializzando le variabili*/
  //definiamo la proprietà per il binding
  selectedHero: Hero;
  heroes: Hero[];
  // in  questo frammento di codice assegniamo l'eroe cliccato dallìutente al componente selectedHero

  constructor(private heroService: HeroService) { }
  //Angular chiama il costruttore dopo aver creato HerosComponent
  ngOnInit() {
    this.getHeroes();
  }
  // permette di recuperare gli eroi
  getHeroes(): void {
    this.heroService.getHeroes().subscribe(heroes => this.heroes = heroes);
  }
  add(name: String): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero).subscribe(hero => { this.heroes.push(hero); });
  }
}
  /*
delete(hero:Hero): void {
  this.heroes= this.heroes.filter(h => h!== hero);
  this.heroService.deleteHero(hero).subscribe();
}


*/